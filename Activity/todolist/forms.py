from django import forms

class LoginForm(forms.Form):
	username = forms.CharField(label='Username', max_length=20)
	password = forms.CharField(label='Password', max_length=20)

class RegistrationForm(forms.Form):
	username = forms.CharField(label='Username', max_length=20)
	firstname = forms.CharField(label='Firstname', max_length=20)
	lastname = forms.CharField(label='Lastname', max_length=20)
	email = forms.CharField(label='Email', max_length=20)
	password = forms.CharField(label='Password', max_length=20)

class AddTaskForm(forms.Form):
	task_name = forms.CharField(label='Task Name', max_length=50)
	description = forms.CharField(label='Description', max_length=200)

class UpdateTaskForm(forms.Form):
	task_name = forms.CharField(label="Task Name", max_length=50)
	description = forms.CharField(label="Description", max_length=200)
	status = forms.CharField(label="status", max_length=50)